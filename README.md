# counter-service-jenkins
Please create a jenkins job and refer below files:

- app.jenkinsfile : 
        to checkout the counter-service code. [https://gitlab.com/wankhede1988/counter-service ]
        Then creating docker Image push it to the ECR repo
        after creating Image it will run "kubectl" command to deploy the application on k8s cluster

-  undateInfra.jenkinsfile
        Check out the code from https://gitlab.com/wankhede1988/counter-service-infra and run </br>
        - TF init
        - TF validate
        - TF plan
        - TF apply

